
The Form Table module provides two new Form API elements and theme functions
which make it easier to format Form elements within HTML tables.

The new element types are:

* `formtable`
* `formrow`

Both behave similarly to the `fieldset` element; you declare a `formtable`
element, then declare `formrow` elements as children of the `formtable`,
and finally you declare your standard form items as children of your
`formrow` elements, using the '#prefix' and '#suffix' attributes to add
opening and closing `<td>` tags.

The `formtable` element accepts the following attributes:
* #attributes
* #title
* #description
* #caption
* #children
* #header - an array of column header strings

The `formrow` element accepts the following attributes:
* #attributes
* #children

(For zebra tables, set the 'class' attribute to 'even' and 'odd' for alternating rows.)

##Usage

  <?php
  
  $form = array();
  
  $form['mytable'] = array(
  	'#type' => 'formtable',
  	'#title' => t('Previous Experience'),
  	'#description' => t('Please tell us about previous positions you have held.'),
  	'#header' => array(t('Title'), t('Company'), t('Salary')) 
  
  $form['mytable']['row1'] = array(
  	'#type' => 'formrow'
  );
  
  $form['mytable']['row1']['title1'] = array(
  	'#type' => 'textfield',
  	'#size' => 20,
  	'#prefix' => '<td>',
  	'#suffix' => '</td>
  );
  
  $form['mytable']['row1']['company1'] = array(
  	'#type' => 'textfield',
  	'#size' => 20,
  	'#prefix' => '<td>',
  	'#suffix' => '</td>
  );
  
  $form['mytable']['row1']['salary1'] = array(
  	'#type' => 'textfield',
  	'#size' => 20,
  	'#prefix' => '<td>',
  	'#suffix' => '</td>
  );  
  
  $form['mytable']['row2'] = array(
  	'#type' => 'formrow',
  	'#prefix' => '<td>',
  	'#suffix' => '</td>
  );
  
  $form['mytable']['row2']['title2'] = array(
  	'#type' => 'textfield',
  	'#size' => 20,
  	'#prefix' => '<td>',
  	'#suffix' => '</td>
  );
  
  $form['mytable']['row2']['company2'] = array(
  	'#type' => 'textfield',
  	'#size' => 20,
  	'#prefix' => '<td>',
  	'#suffix' => '</td>
  );
  
  $form['mytable']['row2']['salary2'] = array(
  	'#type' => 'textfield',
  	'#size' => 20,
  	'#prefix' => '<td>',
  	'#suffix' => '</td>
  );  
  
  /* Et Cetera */
  
  ?>
  
(For a long table you would probably want to just use a loop to generate 
all of the rows and fields, assuming they're sequentially named.)

##Credits
Written & Maintained by Andy Chase <http://proofgroup.com>